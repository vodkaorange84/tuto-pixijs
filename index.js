let Application = PIXI.Application,
    Container = PIXI.Container,
    loader = PIXI.loader,
    resources = PIXI.loader.resources,
    TextureCache = PIXI.utils.TextureCache,
    Sprite = PIXI.Sprite,
    Rectangle = PIXI.Rectangle,
    Graphics = PIXI.Graphics,
    Text = PIXI.Text
    TextStyle = PIXI.TextStyle;


//Create a Pixi Application
let app = new Application({ 
    width: 512, 
    height: 512,                       
    antialiasing: true, 
    transparent: false, 
    resolution: 1,
    backgroundColor: 0x6699FF
  }
);

const NO_COLLISION_TEXT     = "Pas de collision";
const COLLISION_TEXT        = "Collision !";
const BOX_COLLISION_TINT   = 0xFF3300;
const BOX_NO_COLLISION_TINT = 0xCCFF99;

//Add the canvas that Pixi automatically created for you to the HTML document
document.body.appendChild(app.view);
//load an image and run the `setup` function when it's done
loader
  .add("sprites", "images/sprites.json")
  .load(setup);


let gameScene, gameOverScene;

let dungeon, explorer, treasure, door, blobs = [], healtBar;

let treasurePickedByExplorer = false;

let message, state;
function setup() {
  let items = resources.sprites.textures;

  // Game Scene
  gameScene = new Container();
  app.stage.addChild(gameScene);

  gameOverScene = new Container();
  app.stage.addChild(gameOverScene);

  gameOverScene.visible = false;

  // donjon
  dungeon = new Sprite(items["dungeon.png"]);
  gameScene.addChild(dungeon);

  // porte
  door = new Sprite(items["door.png"]);
  door.position.set(32, 0);
  gameScene.addChild(door);

  // explorateur
  explorer = new Sprite(items["explorer.png"]);
  explorer.position.set(68, (gameScene.height / 2 - explorer.height / 2));
  explorer.vx = 0;
  explorer.vy = 0;
  gameScene.addChild(explorer);

  // trésor
  treasure = new Sprite(items["treasure.png"]);
  treasure.position.set((gameScene.width - treasure.width - 48), (gameScene.height / 2 - treasure.height / 2));
  gameScene.addChild(treasure);


  // blob blob
  let nb = 6,
      spacing = 48,
      offset = 150,
      speed = 2,
      direction = 1;
  

  for ( let i = 0; i < nb; i++ ) {
    let blob = new Sprite(items["blob.png"]);

    blob.x = spacing * i + offset;
    blob.y = randomInt(0, app.stage.height - blob.height);

    blob.vy = speed * direction;

    direction *= -1;    // inverse la direction du prochain blob

    blobs.push(blob);
    gameScene.addChild(blob);
  }

  // barre de vie
  healtBar = new Container();
  healtBar.position.set(app.stage.width - 170, 4);
  gameScene.addChild(healtBar);

  let innerBar = new Graphics();
  innerBar.beginFill(0x000000).drawRect(0, 0, 128, 8).endFill();
  healtBar.addChild(innerBar);

  let outerBar = new Graphics();
  outerBar.beginFill(0xFF3300).drawRect(0, 0, 128, 8).endFill();
  healtBar.addChild(outerBar);

  healtBar.outer = outerBar;

  // Construction de l'écran Game Over
  message = new Text("Game Over !", new TextStyle({
    fontFamily: "Futura",
    fontSize: 32, 
    fill: "white"
  }));
  message.position.set(20, app.stage.height / 2 - 32);
  gameOverScene.addChild(message);

  let restartContainer = new Container();
  restartContainer.position.set(180, 300);
  gameOverScene.addChild(restartContainer);

  let button = new Graphics();
  button.beginFill(0xCCCCCC).lineStyle(2, 0x000000, 1);
  button.drawRect(0, 0, 170, 50);
  restartContainer.addChild(button);

  let msgRestart = new Text("Recommencer", new TextStyle({
    fontSize: 22
  }));
  msgRestart.position.set(18, 12);
  restartContainer.addChild(msgRestart);
  restartContainer.interactive = true;
  restartContainer.buttonMode = true;

  restartContainer.on('pointerdown', () => {
    gameOverScene.visible = false;
    gameScene.visibile = true;
    state = play;
  });

  //Capture the keyboard arrow keys
  let left = keyboard("ArrowLeft"),
      up = keyboard("ArrowUp"),
      right = keyboard("ArrowRight"),
      down = keyboard("ArrowDown");
      space = keyboard(" ");

  //Left arrow key `press` method
  left.press = function() {
    explorer.vx = -5;
    explorer.vy = 0;
  };
  //Left arrow key `release` method
  left.release = function() {
    //If the left arrow has been released, and the right arrow isn't down,
    //and the explorer isn't moving vertically:
    //Stop the explorer
    if(!right.isDown && explorer.vy === 0) { explorer.vx = 0; }
  };
  //Up
  up.press = function() {
    explorer.vx = 0;
    explorer.vy = -5;
  };
  up.release = function() {
    if(!down.isDown && explorer.vx === 0) {  explorer.vy = 0; }
  };
  //Right
  right.press = function() {
    explorer.vx = 5;
    explorer.vy = 0;
  };
  right.release = function() {
    if(!left.isDown && explorer.vy === 0) { explorer.vx = 0; }
  };
  //Down
  down.press = function() {
    explorer.vx = 0;
    explorer.vy = 5;
  };
  down.release = function() {
    if ( !up.isDown && explorer.vx === 0) { explorer.vy = 0; }
  };

  space.press = function() {
    treasurePickedByExplorer = !treasurePickedByExplorer;
  }
  space.release = function() {
  }

  state = play;
  app.ticker.add(delta => gameLoop(delta));

}

function gameLoop(delta) {
  state(delta);
}

function play(delta) {
  explorer.x += explorer.vx;
  explorer.y += explorer.vy;

  // bloque le déplacement du personnage dans le conteneur
  contain(explorer, {
    x: 28, y: 10,
    width: 488, height: 480
  });

  let explorerHit = false, explorerWithTreasure = false;

  blobs.forEach(function(blob) {
    blob.y += blob.vy;    // mise en mouvement du blob

    let blobHitsWall = contain(blob, {
      x: 28, y: 18,
      width: 488, height: 480
    });

    if (/top|bottom/.test(blobHitsWall) ) {   // si le blob se cogne en haut ou en bas (utilisation d'une regex)
      blob.vy *= -1;   // reverse de la direction
    }

    if ( hitTestRectangle(explorer, blob) ) {   // si l'explorateur touche un blob
      explorerHit = true;
    }
  });

  if ( explorerHit ) {
    explorer.alpha = 0.5;
    healtBar.outer.width--;
  }
  else {
    explorer.alpha = 1;
  }

  // l'explorateur touche le coffre
  if ( hitTestRectangle(explorer, treasure) ) {
    if ( treasurePickedByExplorer ) {
      treasure.x = explorer.x + 8;
      treasure.y = explorer.y + 8;
    }
    explorerWithTreasure = treasurePickedByExplorer;
  }

  // l'explorateur quitte la pièce
  if ( hitTestRectangle(explorer, door) ) {
    state = end;
    message.text = (explorerWithTreasure) ? "Gagné !" : "Vous êtes sorti sans le coffre !";
  }

  // l'explorateur n'a plus de vie
  if ( healtBar.outer.width < 0) {
    healtBar.outer.width = 0;   // évite que le niveau ne passe en négatif
    state = end;
    message.text = "Perdu !";
  }

}

function end() {
  gameScene.visible = false;
  gameOverScene.visible = true;
}

function contain(sprite, container) {
  let collision = undefined;

  //gauche
  if ( sprite.x < container.x ) {
    sprite.x = container.x;
    collsiion = "left";
  }

  // top
  if ( sprite.y < container.y ) {
    sprite.y = container.y;
    collision = "top";
  }

  // droite
  if ( sprite.x + sprite.width > container.width ) {
    sprite.x = container.width - sprite.width;
    collision = "right";
  }

  // bottom
  if ( sprite.y + sprite.height > container.height ) {
    sprite.y = container.height - sprite.height;
    collision = "bottom";
  }

  return collision;
}



function hitTestRectangle(r1, r2) {

  //Define the variables we'll need to calculate
  let hit, combinedHalfWidths, combinedHalfHeights, vx, vy;

  //hit will determine whether there's a collision
  hit = false;

  //Find the center points of each sprite
  r1.centerX = r1.x + r1.width / 2; 
  r1.centerY = r1.y + r1.height / 2; 
  r2.centerX = r2.x + r2.width / 2; 
  r2.centerY = r2.y + r2.height / 2; 

  //Find the half-widths and half-heights of each sprite
  r1.halfWidth = r1.width / 2;
  r1.halfHeight = r1.height / 2;
  r2.halfWidth = r2.width / 2;
  r2.halfHeight = r2.height / 2;

  //Calculate the distance vector between the sprites
  vx = r1.centerX - r2.centerX;
  vy = r1.centerY - r2.centerY;

  //Figure out the combined half-widths and half-heights
  combinedHalfWidths = r1.halfWidth + r2.halfWidth;
  combinedHalfHeights = r1.halfHeight + r2.halfHeight;

  //Check for a collision on the x axis
  if (Math.abs(vx) < combinedHalfWidths) {

    //A collision might be occuring. Check for a collision on the y axis
    if (Math.abs(vy) < combinedHalfHeights) {

      //There's definitely a collision happening
      hit = true;
    } else {

      //There's no collision on the y axis
      hit = false;
    }
  } else {

    //There's no collision on the x axis
    hit = false;
  }

  //`hit` will be either `true` or `false`
  return hit;
}

function keyboard(value) {
  let key = {};
  key.value = value;
  key.isDown = false;
  key.isUp = true;
  key.press = undefined;
  key.release = undefined;

  // Handler up / down
  key.downHandler = (event) => {
    if ( event.key === key.value ) {
      if ( key.isUp && key.press ) { key.press(); }

      key.isDown = true;
      key.isUp = false;
      event.preventDefault();
    }
  };

  key.upHandler = (event) => {
    if ( event.key === key.value ) {
      if ( key.isDown && key.press ) { key.release(); }

      key.isDown = false;
      key.isUp = true;
      event.preventDefault();
    }
  };

  // attach event listener
  const downListener = key.downHandler.bind(key);
  const upListener = key.upHandler.bind(key);

  window.addEventListener("keydown", downListener, false);
  window.addEventListener("keyup", upListener, false);

  // detach event listener
  key.unsubscribe = () => {
    window.removeEventListener("keydown", downListener);
    window.removeEventListener("keyup", upListener);
  }

  return key;
}

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}